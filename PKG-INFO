Metadata-Version: 2.1
Name: trytond_party_relationship
Version: 7.0.1
Summary: Party Relationship module for Tryton
Home-page: http://www.tryton.org/
Download-URL: http://downloads.tryton.org/7.0/
Author: Tryton
Author-email: foundation@tryton.org
License: GPL-3
Project-URL: Bug Tracker, https://bugs.tryton.org/
Project-URL: Documentation, https://docs.tryton.org/
Project-URL: Forum, https://www.tryton.org/forum
Project-URL: Source Code, https://code.tryton.org/tryton
Keywords: tryton party relationship
Classifier: Development Status :: 5 - Production/Stable
Classifier: Environment :: Plugins
Classifier: Framework :: Tryton
Classifier: Intended Audience :: Developers
Classifier: Intended Audience :: Financial and Insurance Industry
Classifier: Intended Audience :: Legal Industry
Classifier: License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)
Classifier: Natural Language :: Bulgarian
Classifier: Natural Language :: Catalan
Classifier: Natural Language :: Chinese (Simplified)
Classifier: Natural Language :: Czech
Classifier: Natural Language :: Dutch
Classifier: Natural Language :: English
Classifier: Natural Language :: Finnish
Classifier: Natural Language :: French
Classifier: Natural Language :: German
Classifier: Natural Language :: Hungarian
Classifier: Natural Language :: Indonesian
Classifier: Natural Language :: Italian
Classifier: Natural Language :: Persian
Classifier: Natural Language :: Polish
Classifier: Natural Language :: Portuguese (Brazilian)
Classifier: Natural Language :: Romanian
Classifier: Natural Language :: Russian
Classifier: Natural Language :: Slovenian
Classifier: Natural Language :: Spanish
Classifier: Natural Language :: Turkish
Classifier: Natural Language :: Ukrainian
Classifier: Operating System :: OS Independent
Classifier: Programming Language :: Python :: 3
Classifier: Programming Language :: Python :: 3.8
Classifier: Programming Language :: Python :: 3.9
Classifier: Programming Language :: Python :: 3.10
Classifier: Programming Language :: Python :: 3.11
Classifier: Programming Language :: Python :: 3.12
Classifier: Programming Language :: Python :: Implementation :: CPython
Classifier: Topic :: Office/Business
Requires-Python: >=3.8
License-File: LICENSE
Requires-Dist: python-sql>=0.4
Requires-Dist: trytond_party<7.1,>=7.0
Requires-Dist: trytond<7.1,>=7.0
Provides-Extra: test

Party Relationship Module
#########################

The party relationship module allows to define different types of relations
between parties.

Each relation is defined by a relation type. A reverse relation type can be
defined, so  when creating a relation of a type, the reverse relation will be
automatically created.

It is possible to order parties by how closely related they are to a defined
party. The distance is calculated based on the number of steps it takes to get
from the defined party to another. By default all the different types of
relationship are considered, but this can be limited by adding
``relation_usages`` to the context.

Configuration
*************

The party_relationship module use the section ``party_relationship`` to
retrieve some parameters:

- ``depth``: The maximum number of steps to consider when calculating the
  distance between parties.
  The default value is ``7``.
